# Secure Your Wifi (SYW)

[![GitLab license](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://gitlab.com/cjbarker/SecureYourWifi/blob/master/LICENSE.md)

TBA

## Build
``bash
./gradlew build
./gradlew clean
``
