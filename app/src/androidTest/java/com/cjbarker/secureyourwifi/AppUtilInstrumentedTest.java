package com.cjbarker.secureyourwifi;

import android.app.ActivityManager;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class AppUtilInstrumentedTest {

    private static final String APP_TO_LAUNCH = "com.google.android.youtube";

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.cjbarker.secureyourwifi", appContext.getPackageName());
    }

    @Test
    public void test_isAppInstalled() throws Exception {
        Context ctx = InstrumentationRegistry.getTargetContext();
        assertFalse(AppUtil.isAppInstalled(ctx, "monkey.package.is.not.real"));
        assertTrue(AppUtil.isAppInstalled(ctx, ctx.getPackageName()));
    }

    @Test
    public void test_isAppRunning() throws Exception {
        Context ctx = InstrumentationRegistry.getTargetContext();
        assertFalse(AppUtil.isAppRunning(ctx, "monkey.package.is.not.real"));
        assertTrue(AppUtil.isAppRunning(ctx, ctx.getPackageName()));
    }

    @Test
    public void test_launchApp() throws Exception {
        Context ctx = InstrumentationRegistry.getTargetContext();
        assertFalse(AppUtil.launchApp(ctx, "monkey.package.is.not.real"));
        assertTrue(AppUtil.launchApp(ctx, APP_TO_LAUNCH));
    }

    @Test
    public void test_getAppRunning() throws Exception {
        Context ctx = InstrumentationRegistry.getTargetContext();
        ActivityManager.RunningAppProcessInfo app = AppUtil.getRunningApp(ctx, APP_TO_LAUNCH);
        assertNotNull(app);
        assertEquals(app.processName, APP_TO_LAUNCH);
        assertTrue(app.pid > 0);

        app = AppUtil.getRunningApp(ctx, "asdljkasdfoisjs");
        assertNull(app);
    }

}
