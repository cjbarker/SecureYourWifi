/*
 * Copyright 2018. CJ Barker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.cjbarker.secureyourwifi;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.lang.ref.WeakReference;

/**
 * Created by barkc040 on 3/18/18.
 */

public class Settings {

    // class vars
    public static final String LOG_TAG = "Settings";
    private static final String PREFS_NAME = "SYWPrefs";
    private static final String KEY_ENABLE = "enabled";
    private static final String KEY_EVENT_LOG = "logEvents";
    private static final String KEY_NOTIFY = "showNotifications";
    private static final String KEY_DISCONNECT = "disconnectInsecure";
    private static final String KEY_TURNOFF = "turnoffInsecure";
    private static final String KEY_TIMEOUT_DIS = "timeoutDisonnect";
    private static final String KEY_LAST_CHECK = "lastInsecureConnCheck";
    private static final String KEY_VPN_APP_NAME = "vpnAppName";

    // mbr vars
    private WeakReference<Context> ctx;
    private boolean isEnabled;
    private boolean isNotifications;
    private boolean isDisconnect;
    private boolean isTurnoff;
    private boolean isLogEvents;
    private int timeOutDisconnect;
    private long lastInsecureConn;      // timestamp of last insecure connection check
    private String vpnAppName;  // VPN application to launch when insecure connection detected

    private Settings() {
    }

    public Settings(Context ctx) {
        this(ctx, false);
    }

    /**
     * Constructor creates reference to Android context and loads (reads) shared settings if
     * applicable.
     *
     * @param ctx  Android context
     * @param load denotes whether or not to read and load the shared preferences
     */
    public Settings(Context ctx, boolean load) {
        if (ctx == null) {
            Log.w(LOG_TAG, "NullPointer for ctx received in constructor");
            throw new NullPointerException();
        }
        this.ctx = new WeakReference<Context>(ctx);

        if (load) {
            Log.v(LOG_TAG, "Will read and load shared prefs into object");
            this.read();
        }
    }

    /**
     * Reads the shared preferences and loads them into the Settings object
     */
    private void read() {
        Log.v(LOG_TAG, "Reeading Shared Preference: " + this);
        SharedPreferences prefs = this.ctx.get().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        this.isEnabled = prefs.getBoolean(KEY_ENABLE, true);
        this.isLogEvents = prefs.getBoolean(KEY_EVENT_LOG, true);
        this.isNotifications = prefs.getBoolean(KEY_NOTIFY, true);
        this.isDisconnect = prefs.getBoolean(KEY_DISCONNECT, false);
        this.isTurnoff = prefs.getBoolean(KEY_TURNOFF, false);
        this.timeOutDisconnect = prefs.getInt(KEY_TIMEOUT_DIS, 0);
        this.lastInsecureConn = prefs.getLong(KEY_LAST_CHECK, 0);
        this.vpnAppName = prefs.getString(KEY_VPN_APP_NAME, null);
    }

    /**
     * Writes Setting's object member variables to the shared preference
     */
    private void write() {
        Log.v(LOG_TAG, "Writing Shared Preference: " + this);
        SharedPreferences.Editor editor = this.ctx.get()
                .getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putBoolean(KEY_ENABLE, this.isEnabled);
        editor.putBoolean(KEY_EVENT_LOG, this.isLogEvents);
        editor.putBoolean(KEY_NOTIFY, this.isNotifications);
        editor.putBoolean(KEY_DISCONNECT, this.isDisconnect);
        editor.putBoolean(KEY_TURNOFF, this.isTurnoff);
        editor.putInt(KEY_TIMEOUT_DIS, this.timeOutDisconnect);
        editor.putLong(KEY_LAST_CHECK, this.lastInsecureConn);
        editor.putString(KEY_VPN_APP_NAME, this.vpnAppName);
        editor.apply();
    }

    public boolean isDisconnect() {
        return this.isDisconnect;
    }

    public boolean isEnabled() {
        return this.isEnabled;
    }

    public boolean isLogEvents() {
        return this.isLogEvents;
    }

    public boolean isNotifications() {
        return this.isNotifications;
    }

    public boolean isTurnoff() {
        return this.isTurnoff;
    }

    public int getTimeOutDisconnect() {
        return this.timeOutDisconnect;
    }

    public String getVpnAppName() {
        return this.vpnAppName;
    }

    /**
     * Saves (writes) the setting state when the last insecure connection occured.
     * This method is thread safe.
     *
     * @param t timestamp to persist when last insecure connection detected
     */
    public synchronized void setLastInsecureConn(long t) {
        if (t <= 0) {
            return;
        }
        this.lastInsecureConn = t;
        this.write();
    }

    /**
     * Checks if timeout disconnection setting and TTL enabled and determines if it's time
     * to disconnect given TTL expiration.
     *
     * @return True is timeout disconnect enabled and TTL expired else false
     */
    public boolean hasExpiredConn() {
        if (this.timeOutDisconnect <= 0) {
            return false;
        } else if (this.lastInsecureConn <= 0) {
            return false;
        } else {
            long diff = System.currentTimeMillis() - this.lastInsecureConn;
            int diffSecs = (int) diff / 1000;
            return (diffSecs >= this.timeOutDisconnect);
        }
    }

    @Override
    public String toString() {
        return "IsEnabled: " + this.isEnabled + "\n"
                + "IsLogEvents: " + this.isNotifications + "\n"
                + "IsNotifications: " + this.isNotifications + "\n"
                + "IsDisconnect: " + this.isDisconnect + "\n"
                + "IsTurnoff: " + this.isTurnoff + "\n"
                + "TimeOutDisconnect: " + this.timeOutDisconnect + "\n"
                + "VpnAppName: " + this.vpnAppName;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Settings)) {   // NOPMD
            return false;
        } else if (obj == this) {
            return true;
        } else {
            Settings settings = (Settings) obj;
            return this.isEnabled == settings.isEnabled
                    && this.isNotifications == settings.isNotifications
                    && this.isDisconnect == settings.isDisconnect
                    && this.isTurnoff == settings.isTurnoff
                    && this.timeOutDisconnect == settings.timeOutDisconnect
                    && this.vpnAppName.equalsIgnoreCase(((Settings) obj).vpnAppName);
        }
    }

    @Override
    public int hashCode() {
        int hash = 31;
        int enbHash = (this.isEnabled) ? 1 : 0;
        int notHash = (this.isNotifications) ? 1 : 0;
        int disHash = (this.isDisconnect) ? 1 : 0;
        int offHash = (this.isTurnoff) ? 1 : 0;
        hash = 7 * hash + (this.timeOutDisconnect + enbHash + notHash + disHash + offHash + this.vpnAppName.hashCode());
        return hash;
    }
}
