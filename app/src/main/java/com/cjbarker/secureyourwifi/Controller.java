/*
 * Copyright 2018. CJ Barker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.cjbarker.secureyourwifi;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import java.lang.ref.WeakReference;

/**
 * Created by barkc040 on 4/14/18.
 */

public class Controller {

    // class vars
    public static final String LOG_TAG = "Controller";
    public static final String CHANNEL_ID = "SYWIFI-1966";  // Notification Channel Identifier

    // mbr vars
    private WeakReference<Context> ctx;
    private Settings settings;

    /**
     * Preivate constructor require Android context to instantiate
     */
    private Controller() {
    }

    /**
     * Controller constructor that provides reference to Android Context and initializes
     * the {@link com.cjbarker.secureyourwifi.Settings Settings}
     *
     * @param ctx Android context
     */
    public Controller(Context ctx) {
        if (ctx == null) {
            Log.w(LOG_TAG, "NullPointer for ctx received in constructor");
            throw new NullPointerException();
        }

        this.ctx = new WeakReference<Context>(ctx);
        this.settings = new Settings(ctx);
    }

    /**
     * Handles detecting and managing device app settings for given state that accounts for
     * both networking and whether or not the wifi connection is secure over VPN. Will also
     * trigger appropriate Android notification given state and action taken.
     * <p>
     * For example if wifi is enabled, but insecure (no VPN running) it will attempt to launch
     * the VPN app if one is available.
     */
    public void handle() {
        int state = this.getState();

        if (State.hasState(state, State.WIFI_OFF)) {
            Log.i(LOG_TAG, "Wifi is off - nothing to resolve in handle.");
            return;
        }

        // Wifi enabled
        if (State.hasState(state, State.WIFI_SECURE)) {
            Log.i(LOG_TAG, "Wifi is connection is secure - nothing to resolve in handle.");
            return;
        }

        // Wifi enabled and insecure
        if (State.hasState(state, State.VPN_ON)) {
            Log.i(LOG_TAG, "Wifi is on and VPN is already running - nothing to resolve in handle.");
            return;
        }

        // Wifi enabled, insecure and VPN not running
        if (State.hasState(state, State.VPN_APP_INSTALLED)) {
            Log.w(LOG_TAG, "Wifi is insecure, VPN app is installed, but not running");
            Log.i(LOG_TAG, "Launching VPN App: " + settings.getVpnAppName());
            boolean result = AppUtil.launchApp(this.ctx.get(), settings.getVpnAppName());
            EventMgr.writeLog(new EventMgr.EventLog("Wifi was insecure - successfully launched VPN: " + result));
            return;
        }

        // Wifi enabled, insecure, and VPN not running or installed
        if (State.hasState(state, State.VPN_APP_PREFERENCE_SET)) {
            Log.w(LOG_TAG, "Wifi is insecure, VPN preference set, but VPN not running or installed");
            EventMgr.writeLog(new EventMgr.EventLog("Wifi was insecure - missing VPN app to "
                    + "run. Notified user with link to Google Play VPN app"));

            if (!settings.isNotifications()) {
                return;
            }

            // Display notification
            Intent intent = (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + settings.getVpnAppName())));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this.ctx.get(), 0, intent, 0);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this.ctx.get(), CHANNEL_ID)
                    .setContentTitle(this.ctx.get().getString(R.string.app_name))
                    .setContentText("Wifi insecure, missing VPN app, install via Google Play")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);


            NotificationManagerCompat nm = NotificationManagerCompat.from(this.ctx.get());
            nm.notify(8675302, builder.build());

            return;
        } else {
            NetworkUtil.resolveInsecureWifi(this.ctx.get(), this.settings);
            return;
        }
    }

    /**
     * Determines and returns the device's given state: wifi on|off, wifi in|secure, VPN app
     * un|install, and VPN app on|off.
     *
     * @return Bit flag that denotes the current device
     * {@link com.cjbarker.secureyourwifi.Controller.State State}
     */
    private int getState() {
        Log.v(LOG_TAG, "Get Device's Controller/Flag...");

        int value = 0;
        if (this.ctx == null) {
            throw new NullPointerException();
        }

        // Determine network state
        NetworkUtil.ConnectionType connType = NetworkUtil.getConnectivityType(this.ctx.get());

        value = (connType == NetworkUtil.ConnectionType.WIFI) ? (value | State.WIFI_ON) : (value | State.WIFI_OFF);

        if (State.hasState(value, State.WIFI_ON)) {
            value = (NetworkUtil.isWifiSecure(this.ctx.get())) ? (value | State.WIFI_SECURE) : (value | State.WIFI_INSECURE);
        }

        // Determine VPN state
        String vpnAppName = this.settings.getVpnAppName();

        if (vpnAppName == null || vpnAppName.trim().equals("")) {
            Log.d(LOG_TAG, "No VPN APP NAME set in preferences");
            return value;
        }

        value = (value | State.VPN_APP_PREFERENCE_SET);
        Log.d(LOG_TAG, "Preferred VPN App Name: " + vpnAppName);

        value = (AppUtil.isAppInstalled(this.ctx.get(), vpnAppName)) ? (value | State.VPN_APP_INSTALLED) : (value | State.VPN_APP_NOT_INSTALLED);

        if (State.hasState(value, State.VPN_APP_INSTALLED)) {
            Log.d(LOG_TAG, "VPN App installed: " + vpnAppName);
            value = (AppUtil.isAppRunning(this.ctx.get(), vpnAppName)) ? (value | State.VPN_ON) : (value | State.VPN_OFF);
        }

        return value;
    }

    /**
     * Inner class of state provides integer bit flags denoting the current state of the device in
     * the context of wifi and vpn.
     */
    static class State {

        public static final int WIFI_ON = 1;                    // Binary 000000001
        public static final int WIFI_OFF = 2;                   // Binary 000000010
        public static final int WIFI_SECURE = 4;                // Binary 000000100
        public static final int WIFI_INSECURE = 8;              // Binary 000001000
        public static final int VPN_APP_PREFERENCE_SET = 16;    // Binary 000010000
        public static final int VPN_APP_NOT_INSTALLED = 32;     // Binary 000100000
        public static final int VPN_APP_INSTALLED = 64;         // Binary 001000000
        public static final int VPN_ON = 128;                   // Binary 010000000
        public static final int VPN_OFF = 256;                  // Binary 100000000

        /**
         * Checks if given integer value has a given bit flag's state.
         *
         * @param value        Value to check for state
         * @param valueToCheck State to check for
         * @return True if state is enabled for the given value else false
         */
        public static boolean hasState(int value, int valueToCheck) {
            Log.v(LOG_TAG, "hasState " + valueToCheck + " within existing value " + value);

            if (value == 0) {
                Log.d(LOG_TAG, "Value not set will not check for state");
                return false;
            } else {
                boolean result = ((value & valueToCheck) == valueToCheck);
                Log.d(LOG_TAG, "hasState " + valueToCheck + ": " + result);
                return result;
            }
        }
    }
}
