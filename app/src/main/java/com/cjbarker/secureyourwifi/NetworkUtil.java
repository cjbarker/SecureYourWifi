/*
 * Copyright 2018. CJ Barker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.cjbarker.secureyourwifi;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import java.util.List;

/**
 * Created by barkc040 on 3/18/18.
 */
class NetworkUtil {

    // class vars
    public static final String LOG_TAG = "NetworkUtil";

    // member vars
    private ConnectionType connType;
    private long lastCheck;

    /**
     * Retreives the network connection type for the given Android context.
     *
     * @param ctx Android context
     * @return ConnectionType for the active network
     */
    static ConnectionType getConnectivityType(final Context ctx) {
        if (ctx == null) {
            Log.w(LOG_TAG, "NullPointer for ctx received in getConnectivityType");
            throw new NullPointerException();
        }

        ConnectivityManager cm = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null) {
            return ConnectionType.fromInt(activeNetwork.getType());
        }

        return ConnectionType.UNKNOWN;
    }

    /**
     * If connected to wifi will return the BSSID connection name
     *
     * @param ctx Android Context
     * @return BSSID connected to or null if no wifi connection
     */
    static boolean isWifiSecure(final Context ctx) {
        if (ctx == null) {
            Log.w(LOG_TAG, "NullPointer for ctx received in isWifiSecure");
            throw new NullPointerException();
        }

        Log.v(LOG_TAG, "isWifiSecure");

        WifiManager wifiMgr = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);

        if (!wifiMgr.isWifiEnabled()) {
            Log.d(LOG_TAG, "WIfi is not enabled - unable to check if secure");
            return false;
        }

        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        Wifi wifi = new Wifi(wifiInfo);

        // Must have completed, authenticated connection
        if (!wifi.IsAuthenticated) {
            Log.d(LOG_TAG, "Wifi connection incomplete - not yet authenticated");
            Log.i(LOG_TAG, "Wifi connection not esablished try again later.");
            return false;
        }

        // Scan available networks and check if connected SSID is at WPA2
        List<ScanResult> networkList = wifiMgr.getScanResults();

        if (networkList == null) {
            Log.d(LOG_TAG, "No wifi network points (APs) found for scan result");
            return false;
        }

        for (ScanResult network : networkList) {
            if (wifi.Ssid.equals(network.SSID)) {
                String capabilities = network.capabilities;
                Log.v(LOG_TAG, "Network SSID: " + network.SSID + " capabilities: " + capabilities);

                if (capabilities.contains("WPA2")) {
                    return true;
                } else if (capabilities.contains("WPA")) {
                    return false;
                } else if (capabilities.contains("WEP")) {
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * Attempts to resolve an insecure wifi connection for the given Android context based on the
     * user's application settings. Handles sending notification to the device if/when insecure
     * wifi connection detected.
     *
     * @param ctx      Android context
     * @param settings {@link com.cjbarker.secureyourwifi.Settings Settings} of the device
     */
    static void resolveInsecureWifi(final Context ctx, Settings settings) {
        if (ctx == null) {
            Log.e(LOG_TAG, "NullPointer for ctx received in resolveInsecureWifi");
            return;
        }

        if (settings == null) {
            Log.w(LOG_TAG, "Null pointer to Settings - will read/reload");
            //CHECKSTYLE:OFF
            settings = new Settings(ctx, true);
            //CHECKSTYLE:ON
        }

        // Insecure wifi connection apply appropriate fix based on settings
        Log.v(LOG_TAG, "Wifi connection is insecure - will attempt to resolve");

        // Buffer to notification builder msg
        StringBuffer buf = new StringBuffer();
        WifiManager wifiMgr = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        NetworkUtil.Wifi wifi = new NetworkUtil.Wifi(wifiInfo);

        if (settings.isTurnoff()) {
            Log.i(LOG_TAG, "Turning off wifi due to insecure connection");

            if (wifiMgr.setWifiEnabled(false)) {
                Log.i(LOG_TAG, "Successfully disabled wifi - toggled off");
                buf.append("Wifi turned off due to insecure connection on SSID: " + wifi.Ssid);
            } else {
                Log.w(LOG_TAG, "Failed to disable wifi due to insecure SSID: " + wifi.Ssid);
            }
        } else if (settings.getTimeOutDisconnect() > 0) {
            settings.setLastInsecureConn(System.currentTimeMillis());

            if (settings.hasExpiredConn()) {
                Log.d(LOG_TAG, "Insecure connection TTL is " + settings.getTimeOutDisconnect() + " seconds");
                Log.i(LOG_TAG, "Insecure TTL has expired. Disconnected from wifi on SSID: " + wifi.Ssid);

                if (wifiMgr.disconnect()) {
                    Log.i(LOG_TAG, "Successfully disconnected from SSID " + wifi.Ssid);
                    buf.append("Insecure connection timeout. Disconnected from access point on SSID: " + wifi.Ssid);
                } else {
                    Log.w(LOG_TAG, "Failed to disconnect from SSID: " + wifi.Ssid);
                }
            } else {
                Log.d(LOG_TAG, "Insecure connection has not expired TTL yet of seconds: " + settings.getTimeOutDisconnect());
                // TODO trigger alarm or thread to check again in X seconds
            }
        } else if (settings.isDisconnect()) {
            Log.i(LOG_TAG, "Wifi disconnected from insecure connection on SSID: " + wifi.Ssid);

            if (wifiMgr.disconnect()) {
                Log.i(LOG_TAG, "Successfully disconnected from SSID " + wifi.Ssid);
                buf.append("Wifi disconnected from Access Point due insecure connection on SSID: " + wifi.Ssid);
            } else {
                Log.w(LOG_TAG, "Failed to disconnect from SSID: " + wifi.Ssid);
            }
        } else {
            // Insecure connection, service enabled, but settings disabled
            Log.w(LOG_TAG, "Should have some settings - not clear how possible.");
            Log.i(LOG_TAG, "Service enabled, but no settings found");
            buf.append("Insecure wifi connection. Need settings preference to resolve.");
            // If this is occurring perhaps just turn off wifi?
            // wifiMgr.setWifiEnabled(false)
        }

        EventMgr.writeLog(new EventMgr.EventLog(buf.toString()));

        // Send applicable notification
        if (settings.isNotifications() && buf.length() > 0) {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(ctx, Controller.CHANNEL_ID)
                    .setContentTitle(ctx.getString(R.string.app_name))
                    .setContentText(buf.toString())
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

            NotificationManagerCompat nm = NotificationManagerCompat.from(ctx);
            nm.notify(8675301, builder.build());
        }
    }

    /**
     * Enum defines the network connection state given an Android's int state.
     */
    public enum ConnectionType {

        WIFI(1),
        MOBILE(2),
        NOT_CONNECTED(0),
        UNKNOWN(666);

        private final int state;

        /**
         * Constructs the ConnetionType for a given Android int state
         *
         * @param state Android network state
         */
        ConnectionType(int state) {
            switch (state) {
                case 0:
                case 1:
                case 2:
                    this.state = state;
                    break;
                default:
                    this.state = 666;
            }
        }

        /**
         * Converts integer state to enum ConnectionType
         *
         * @param x android's int network connection state
         * @return ConnectionType given the Android int or UNKNOWN
         */
        public static ConnectionType fromInt(int x) {
            switch (x) {
                case 0:
                    return NOT_CONNECTED;
                case 1:
                    return WIFI;
                case 2:
                    return MOBILE;
                default:
                    return UNKNOWN;
            }
        }

        private int state() {
            return this.state;
        }

        /**
         * Converts connection type to a string.
         *
         * @return String description of connection type
         */
        public String getStatusString() {
            String status = null;
            switch (this.state) {
                case 0:
                    status = "Not connected to Internet";
                    break;
                case 1:
                    status = "Wifi enabled";
                    break;
                case 2:
                    status = "Mobile data enabled";
                    break;
                default:
                    status = "Unknown Network Status";
                    break;
            }
            return status;
        }
    }

    /**
     * POJO representing a given connected wifi access point state and client's connection
     */
    static class Wifi {

        // class vars
        public static final String LOG_TAG = "Wifi";

        // mbr vars
        //CHECKSTYLE:OFF
        public final String MacAddress;
        public final String Bssid;
        public final String Ssid;
        public final int Rssi;
        public final int IpAddr;
        public final boolean IsAuthenticated;
        //CHECKSTYLE:ON

        /**
         * Constructions the Wifi POJO based on Android's WifiINfo
         *
         * @param wifiInfo Android object to parse and load into POJO
         */
        Wifi(WifiInfo wifiInfo) {
            if (wifiInfo == null) {
                Log.w(LOG_TAG, "NullPointer for ctx received in constructor");
                throw new NullPointerException();
            }
            this.MacAddress = wifiInfo.getMacAddress();
            this.Bssid = wifiInfo.getBSSID();
            this.Ssid = wifiInfo.getSSID();
            this.Rssi = wifiInfo.getRssi();
            this.IpAddr = wifiInfo.getIpAddress();
            this.IsAuthenticated = (wifiInfo.getSupplicantState() == SupplicantState.COMPLETED);
        }
    }
}
