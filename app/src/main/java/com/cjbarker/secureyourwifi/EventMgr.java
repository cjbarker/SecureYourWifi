/*
 * Copyright 2018. CJ Barker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.cjbarker.secureyourwifi;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Manages all event logs of the application: reading, writing, and clearing.
 * <p>
 * Created by barkc040 on 3/19/18.
 */

public final class EventMgr {

    // class vars
    public static final String LOG_TAG = "EventLog";
    private static final Object LOCK = new Object();

    private EventMgr() {
    }

    static boolean clearLogs() {
        boolean success = false;

        // TODO check if have any record counts (no lock) if no escape if yes clear

        synchronized (LOCK) {
            // TODO delete all records from database
            success = true;
        }

        return success;
    }

    static List<EventLog> getLogs() {
        List<EventLog> logs = null;

        logs = new ArrayList<>();

        // TODO read logs from database and sort by descending

        return logs;
    }

    static boolean writeLog(EventLog log) {
        boolean success = false;

        if (log == null || log.msg.trim().equals("")) {
            Log.w(LOG_TAG, "Null pointer or no log for writeLog");
            return success;
        }

        // TODO write to database
        synchronized (LOCK) {
            Log.v(LOG_TAG, "Writing EventLog to database: " + log);
            success = true;
        }

        return success;
    }

    static boolean writeLogs(List<EventLog> logs) {
        boolean success = false;

        if (logs == null || logs.size() <= 0) {
            Log.w(LOG_TAG, "Null pointer or no logs for writeLogs");
            return success;
        }

        success = true;

        for (EventLog log : logs) {
            if (log.msg.trim().equals("")) {
                Log.d(LOG_TAG, "Empty log message will not write");
                continue;
            }

            // TODO write log to DB or do it in bulk
            if (!writeLog(log)) {
                success = false;
            }
        }

        return success;
    }

    static class EventLog {

        //CHECKSTYLE:OFF
        public long timestamp;
        public String msg = null;
        //CHECKSTYLE:ON

        private EventLog() {
        }

        public EventLog(String msg) {
            this(System.currentTimeMillis(), msg);
        }

        public EventLog(long timestamp, String msg) {
            this.timestamp = timestamp;
            this.msg = msg;
        }

        @Override
        public String toString() {
            // TODO format date to display
            return (timestamp + " - " + msg);
        }
    }
}
