/*
 * Copyright 2018. CJ Barker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.cjbarker.secureyourwifi;

import android.app.ActivityManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Process;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by barkc040 on 4/14/18.
 */

public final class AppUtil {

    public static final String LOG_TAG = "AppUtil";

    private AppUtil() {
    }

    /**
     * Launches given application.
     *
     * @param ctx         Android context
     * @param packageName fully qualified package name of app to launch
     * @return true if app launched else false
     */
    public static boolean launchApp(Context ctx, String packageName) {
        Log.v(LOG_TAG, "launchApp");

        if (ctx == null || packageName == null || packageName.trim().equals("")) {
            Log.w(LOG_TAG, "Null pointer on context or packageName");
            return false;
        }

        PackageManager pm = ctx.getPackageManager();

        try {
            Intent i = pm.getLaunchIntentForPackage(packageName);
            if (i == null) {
                Log.w(LOG_TAG, "Unable to get intent for package name: " + packageName);
                return false;
            }
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            ctx.startActivity(i);
            Log.i(LOG_TAG, "Successfully launched package: " + packageName);
            return true;
        } catch (ActivityNotFoundException e) {
            Log.e(LOG_TAG, "Unable launch activity not found for package: " + packageName);
            return false;
        }
    }

    /**
     * Checks if given application's process is running on the device.
     *
     * @param ctx     Android context
     * @param appName Application name to check process
     * @return true if application proc is running else false
     */
    public static boolean isAppRunning(Context ctx, String appName) {
        ActivityManager.RunningAppProcessInfo runningApp = getRunningApp(ctx, appName);
        return (runningApp != null && runningApp.pid > 0) ? true : false;
    }

    /**
     * Checks running processes on the device and returns process for given app name if found
     *
     * @param ctx     Android context
     * @param appName Application name of process to check if running
     * @return Process found running or null if not found
     */
    public static ActivityManager.RunningAppProcessInfo getRunningApp(Context ctx, String appName) {
        Log.v(LOG_TAG, "isAppRunning: " + appName);

        if (ctx == null) {
            throw new NullPointerException();
        }

        ActivityManager.RunningAppProcessInfo proc = null;

        if (appName == null || appName.trim().equals("")) {
            Log.w(LOG_TAG, "Null pointer for appName in isAppRunning");
            return proc;
        }

        List<ActivityManager.RunningAppProcessInfo> procs = getRunningApps(ctx);

        if (procs == null || procs.size() <= 0) {
            Log.d(LOG_TAG, "App is not running: " + appName);
            return proc;
        }

        int idx = Collections.binarySearch(procs, appName, new Comparator() {
            @Override
            public int compare(Object proc1, Object proc2) {
                String procName1 = ((ActivityManager.RunningAppProcessInfo) proc1).processName;
                String procName2 = ((ActivityManager.RunningAppProcessInfo) proc2).processName;
                return procName1.compareTo(procName2);
            }
        });

        if (idx >= 0) {
            proc = procs.get(idx);
            Log.d(LOG_TAG, "Process [" + proc.processName + "] is running at pid: " + proc.processName);
        }

        return proc;
    }

    /**
     * Stops (kills) a given process
     *
     * @param pid Process ID to kill
     */
    public static void stopApp(int pid) {
        if (pid <= 0) {
            Log.w(LOG_TAG, "Invalid process id passed to stop app: " + pid);
            return;
        }
        // only kills PID if started from same UID as running app/pid
        Process.killProcess(pid);
    }

    /**
     * Checks device for all running processes (apps)
     *
     * @param ctx Android Context
     * @return List of running processes on the device
     */
    private static List<ActivityManager.RunningAppProcessInfo> getRunningApps(Context ctx) {
        Log.v(LOG_TAG, "getRunningApps");

        ActivityManager activityManager
                = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> procInfos
                = activityManager.getRunningAppProcesses();

        // sort alphabetically
        Collections.sort(procInfos, new Comparator<ActivityManager.RunningAppProcessInfo>() {
            @Override
            public int compare(ActivityManager.RunningAppProcessInfo proc1,
                               ActivityManager.RunningAppProcessInfo proc2) {
                return proc1.processName.compareTo(proc2.processName);
            }
        });

        return procInfos;
    }

    /**
     * Checks if given application is installed on the device.
     *
     * @param ctx     Android context
     * @param appName Application name to check for
     * @return true if app is installed on device else false
     */
    public static boolean isAppInstalled(Context ctx, String appName) {
        Log.v(LOG_TAG, "isAppInstalled: " + appName);

        if (ctx == null) {
            throw new NullPointerException();
        }
        if (appName == null || appName.trim().equals("")) {
            Log.w(LOG_TAG, "Null pointer for appName in isAppInstalled");
            return false;
        }

        List<String> apps = getInstalledApps(ctx, true);

        if (apps == null || apps.size() <= 0) {
            Log.d(LOG_TAG, "App is NOT installed: " + appName);
            return false;
        }

        boolean isInstalled = (Collections.binarySearch(apps, appName) >= 0);
        Log.d(LOG_TAG, "App [" + appName + "] is installed: " + isInstalled);
        return isInstalled;
    }

    /**
     * Gets list of application names installed on device
     *
     * @param ctx              Android context
     * @param ignoreSystemPkgs Denotes whether or not to ignore system packages installed
     * @return List of packages installed on device
     */
    private static List<String> getInstalledApps(Context ctx, boolean ignoreSystemPkgs) {
        if (ctx == null) {
            throw new NullPointerException();
        }

        List<PackageInfo> packList = ctx.getPackageManager().getInstalledPackages(0);
        List<String> apps = null;
        String appName = null;

        for (int i = 0; i < packList.size(); i++) {
            PackageInfo packInfo = packList.get(i);
            appName = packInfo.applicationInfo.loadLabel(ctx.getPackageManager()).toString();

            if (ignoreSystemPkgs && (packInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
                Log.d(LOG_TAG, "System Application ignored for package: " + appName);
                continue;
            }

            if (apps == null) {
                apps = new ArrayList<>();
            }

            Log.d(LOG_TAG, "Installed App: " + appName);
            apps.add(appName);
        }

        // sort alphabetically
        Collections.sort(apps, new Comparator<String>() {
            @Override
            public int compare(String appName1, String appName2) {
                return appName1.compareTo(appName2);
            }
        });

        return apps;
    }
}
