/*
 * Copyright 2018. CJ Barker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.cjbarker.secureyourwifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Debug;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by barkc040 on 3/19/18.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    // class vars
    public static final String LOG_TAG = "NetChangeRecv";

    /**
     * Receiver of any network change notification via Android.
     * Will handle resolving the state of the device if wifi connection is insecure (wifi enabled,
     * but no VPN running) via {@link com.cjbarker.secureyourwifi.NetworkUtil #resolveInsecureWifi}
     *
     * @param ctx    Android context
     * @param intent Android intent
     */
    @Override
    public void onReceive(final Context ctx, final Intent intent) {
        Log.v(LOG_TAG, "Network broadcast received");

        Settings settings = new Settings(ctx, true);

        if (!settings.isEnabled()) {
            Log.i(LOG_TAG, "SecureYourWifi service is NOT enabled.");
            return;
        }

        WifiManager wifiMgr = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);

        if (!wifiMgr.isWifiEnabled()) {
            Log.i(LOG_TAG, "Wifi is not enabled on the device.");
            return;
        }

        // Wifi Enabled - determine state
        NetworkUtil.ConnectionType connType = NetworkUtil.getConnectivityType(ctx);
        String status = connType.getStatusString();

        Log.d(LOG_TAG, "Connection type " + status);

        if (BuildConfig.DEBUG || Debug.isDebuggerConnected()) {
            Toast.makeText(ctx, status, Toast.LENGTH_LONG).show();
        }

        if (connType != NetworkUtil.ConnectionType.WIFI) {
            Log.d(LOG_TAG, "No wifi connection status - nothing to check.");
            return;
        }

        // Connected to wifi
        if (NetworkUtil.isWifiSecure(ctx)) {
            Log.d(LOG_TAG, "Wifi connection is secure. Nothing further to do.");
            return;
        }

        // Handle insecure connection accordingly
        NetworkUtil.resolveInsecureWifi(ctx, settings);
    }
}
